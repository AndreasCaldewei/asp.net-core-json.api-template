﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectHighlander.Models;

namespace ProjectHighlander.Controllers
{
    [Route("[controller]")]
    public class UserController: JsonApiController<User>
    {
        public UserController(
            IJsonApiContext jsonApiContext,
            IResourceService<User> resourceService)
            : base(jsonApiContext, resourceService) { }


    }
}
