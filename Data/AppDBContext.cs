﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectHighlander.Models;

namespace ProjectHighlander.Data
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) :  base(options)
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
