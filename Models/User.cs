﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonApiDotNetCore.Models;

namespace ProjectHighlander.Models
{
    public class User : Identifiable
    {

        [Attr("name")]
        public string Name { get; set; }

    }
}
